﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    Rigidbody2D ballRigidBody;
    [SerializeField] Player player;
    [SerializeField] float randomFactor = 0.2f;

    [SerializeField] float xPush = 2f;
    [SerializeField] float minYPush = 15f;
    [SerializeField] float maxYPush = 20f;
    private float yPush;

    [SerializeField] GameObject ball;

    LevelHandler levelHandler;


    // Start is called before the first frame update
    void Start()
    {
        ballRigidBody = GetComponent<Rigidbody2D>();
        player = FindObjectOfType<Player>();
        levelHandler = FindObjectOfType<LevelHandler>();

        yPush = Random.Range(minYPush, maxYPush);
        if(levelHandler.checkWinCondition() == true)
        {
            yPush *= -1;
        }

        ballRigidBody.velocity = new Vector2(Random.Range(-xPush, xPush), yPush);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    private void OnCollisionEnter2D(Collision2D collision)
    {
        Vector2 velocityTweek = new Vector2(Random.Range(0f, randomFactor), Random.Range(0f, randomFactor));

        ballRigidBody.velocity += velocityTweek;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "PlayerLoseCollider")
        {
            levelHandler.IncreaseEnemyScore();
            levelHandler.CreateNewBall();
            Destroy(this.gameObject);
        }else if (collision.tag == "EnemyLoseCollider")
        {
            levelHandler.IncreasePlayerScore();
            levelHandler.CreateNewBall();
            Destroy(this.gameObject);
        }
    }

}
