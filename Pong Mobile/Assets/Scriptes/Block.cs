﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block : MonoBehaviour
{
    [SerializeField] Sprite[] hitSprites;
    [SerializeField] int timesHit;

    [SerializeField] float blockMoveSpeed = 2f;

    Rigidbody2D blockRB;
    LevelHandler levelHandler;

    // Start is called before the first frame update
    void Start()
    {
        blockRB = GetComponent<Rigidbody2D>();
        levelHandler = FindObjectOfType<LevelHandler>();
    }

    // Update is called once per frame
    void Update()
    {
        if (levelHandler.blockMovement == true)
        {
            blockRB.velocity = new Vector2(-1 * blockMoveSpeed, 0);
        }
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (tag == "Breakable")
        {
            HandleHit();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Destroy(gameObject);
    }

    private void HandleHit()
    {
        timesHit++;
        int maxHits = hitSprites.Length + 1;
        if (timesHit >= maxHits)
        {
            Destroy(gameObject);
        }
        else
        {
            ShowNextHitSprites();
        }
    }

    private void ShowNextHitSprites()
    {
        int spriteIndex = timesHit - 1;
        if (hitSprites[spriteIndex] != null)
        {
            GetComponent<SpriteRenderer>().sprite = hitSprites[spriteIndex];
        }
        else
        {
            Debug.LogError("Block sprite is missing from array." + gameObject.name);
        }
    }
}
