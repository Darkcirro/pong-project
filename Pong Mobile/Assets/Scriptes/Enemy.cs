﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField] float screenWidth = 16f;
    [SerializeField] float minX = 1.5f;
    [SerializeField] float maxX = 16.5f;
    [SerializeField] float speed = 2f;

    Touch touch;
    Ball theBall;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        theBall = FindObjectOfType<Ball>();
        if(theBall != null)
        {
            Vector2 paddlePos = new Vector2(transform.position.x, transform.position.y);
            paddlePos.x = Mathf.Clamp(GetXPos(), minX, maxX);
            if (paddlePos.x < transform.position.x - 0.4 || paddlePos.x > transform.position.x + 0.4)
            {
                transform.position = Vector2.MoveTowards(transform.position, paddlePos, speed * Time.deltaTime);
            }
        }
    }

    private float GetXPos()
    {
        return theBall.transform.position.x;
    }
}
