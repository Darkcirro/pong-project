﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class LevelHandler : MonoBehaviour
{
    private int playerCurrentScore = 0;
    private int enemyCurrentScore = 0;

    private int playerLastScore;
    private int enemyLastScore;

    private float timeBeforeStart = 3f;
    private bool checkGameStart = false;

    [SerializeField] int maxScore = 5;

    [Header("Show after Countdown")]
    [SerializeField] GameObject ball;
    [SerializeField] GameObject playSpace;
    [SerializeField] GameObject blocks;

    [Header("Text UI")]
    [SerializeField] Text playerScore;
    [SerializeField] Text enemyScore;
    [SerializeField] Text playerWinText;
    [SerializeField] Text enemyWinText;
    [SerializeField] Text timeBeforeStartText;

    [Header("Block Control")]
    [SerializeField] GameObject blockPrefeb;
    [SerializeField] float blockSpawnRate = 3f;
    public bool blockMovement = false;
    private float blockSpawnTimer;
    Block block;

    SceneLoader sceneLoader;
    

    // Start is called before the first frame update
    void Start()
    {
        sceneLoader = FindObjectOfType<SceneLoader>();
        block = FindObjectOfType<Block>();

        playerScore.text = playerCurrentScore.ToString();
        enemyScore.text = enemyCurrentScore.ToString();
        playerLastScore = playerCurrentScore;
        enemyLastScore = enemyCurrentScore;
        blockSpawnTimer = blockSpawnRate;
    }

    // Update is called once per frame
    void Update()
    {
        CountDownBeforeStart();
        if(blockMovement == true)
        {
            SpawnBlock();
        }
        haveWinner();
    }

    private void SpawnBlock()
    {
        blockSpawnTimer -= Time.deltaTime;
        if(blockSpawnTimer <= 0)
        {
            blockSpawnTimer += blockSpawnRate;
            var newBlock = Instantiate(blockPrefeb, new Vector2(20f, 17.5f), Quaternion.identity);
            newBlock.transform.SetParent(blocks.transform);
        }
    }

    private void CountDownBeforeStart()
    {
        if (checkGameStart == true)
            return;

        timeBeforeStartText.text = timeBeforeStart.ToString();
        timeBeforeStart -= Time.deltaTime;
        if(timeBeforeStart <= 0)
        {
            Destroy(timeBeforeStartText);
            playerScore.gameObject.SetActive(true);
            enemyScore.gameObject.SetActive(true);
            playSpace.gameObject.SetActive(true);

            if(blocks != null)
            {
                blocks.gameObject.SetActive(true);
            }
            else
            {
                Debug.Log("Don't have Blocks or missing Blocks.");
            }

            Instantiate(ball, new Vector2(9f, 16f), Quaternion.identity);

            checkGameStart = true;
        }
    }

    public void CreateNewBall()
    {
        if (playerCurrentScore != maxScore && enemyCurrentScore != maxScore)
        {
            StartCoroutine(WaitToCreateNewBall());
        }
    }

    IEnumerator WaitToCreateNewBall()
    {
        yield return new WaitForSeconds(2f);
        Instantiate(ball, new Vector2(9f, 17.5f), Quaternion.identity);
    }

    public void IncreasePlayerScore()
    {
        playerCurrentScore++;
        playerScore.text = playerCurrentScore.ToString();
    }

    public void IncreaseEnemyScore()
    {
        enemyCurrentScore++;
        enemyScore.text = enemyCurrentScore.ToString();
    }

    public void haveWinner()
    {
        if(playerCurrentScore == maxScore)
        {
            playerWinText.gameObject.SetActive(true);
            if (Input.GetMouseButton(0))
            {
                sceneLoader.LoadNextStage();
            }
        }
        else if (enemyCurrentScore == maxScore)
        {
            enemyWinText.gameObject.SetActive(true);
            if (Input.GetMouseButton(0))
            {
                sceneLoader.LoadMainMenu();
            }
        }
    }

    public bool checkWinCondition()
    {
        bool checkGetScore = true;
        if(playerLastScore != playerCurrentScore)
        {
            checkGetScore = true;
            playerLastScore = playerCurrentScore;
        }
        if(enemyLastScore != enemyCurrentScore)
        {
            checkGetScore = false;
            enemyLastScore = enemyCurrentScore;
        }

        return checkGetScore;
    }

}
